<?php
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Task\Entity\Task as TaskModel;
use Task\Value\Type;

/**
 * Tasks Fixtures
 * @author Thiago Paes <mrprompt@gmail.com>
 */
class Task extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Loader
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $i = 1;

        while ($i <= 9) {
            $task = [
                'uuid'      => 'd318880a-980f-4d34-98ab-c735bfb22c6' . $i,
                'type'      => 'work',
                'content'   => uniqid(),
                'order'     => $i,
                'done'      => $i === 3 ? true : false,
                'deleted'   => $i === 5 ? true : false,
            ];

            $obj = new TaskModel();
            $obj->setContent($task['content']);
            $obj->setType(new Type($task['type']));
            $obj->setOrder($task['order']);
            $obj->done($task['done']);
            $obj->delete($task['deleted']);
            $obj->setUuid($task['uuid']);

            $manager->persist($obj);

            $i++;
        }

        $manager->flush();

        $this->addReference('task_' . $i, $obj);
    }

    /**
     * Load order
     *
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
