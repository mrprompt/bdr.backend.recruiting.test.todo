<?php
declare(strict_types = 1);

namespace Task\Tests\Controller;

use Common\Response as View;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\ORM\Tools\SchemaTool;
use Silex\WebTestCase;

/**
 * Task Controller Test Case
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
class TaskTest extends WebTestCase
{
    use TaskProvider;

    /**
     * @var array
     */
    protected $header = [
        'HTTP_Content-type' => 'application/json'
    ];

    /**
     * Test bootstrap
     *
     * @return \Silex\Application
     */
    public function createApplication()
    {
        /* @var $app \Silex\Application */
        $app = include __DIR__ . '/../../../../bootstrap.php';

        /* @var $metadata array */
        $metadata = $app['doctrine_orm.em']->getMetadataFactory()->getAllMetadata();

        /* @var $tool SchemaTool */
        $tool = new SchemaTool($app['doctrine_orm.em']);
        $tool->createSchema($metadata);

        /* @var $loader Loader */
        $loader = new Loader();
        $loader->loadFromDirectory(__DIR__ . '/../../../../fixtures');

        /* @var $executor ORMExecutor */
        $executor = new ORMExecutor($app['doctrine_orm.em']);
        $executor->execute($loader->getFixtures(), true);

        $app['logger']->addDebug('Fixtures loaded to ' . static::class);

        return $this->app = $app;
    }

    /**
     * @test
     */
    public function getHomeTaskMustBeListAllTasks()
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('GET', '/task/');

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(View::HTTP_OK, $object->code);
        $this->assertInternalType('array', $object->data);
    }

    /**
     * @test
     * @param string $order
     * @dataProvider validOrders
     */
    public function getHomeWithOrdQueryStringTaskMustBeListAllTasks($order)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('GET', '/task/?ord=' . $order);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(View::HTTP_OK, $object->code);
        $this->assertInternalType('array', $object->data);
    }

    /**
     * @test
     */
    public function getHomeWithZeroTasksMustBeResultMessage()
    {
        // deleting all tasks before request
        $em = $this->app['doctrine_orm.em'];
        $query = $em->createQuery('update Task\Entity\Task t set t.deleted = true where t.id > 0');
        $query->execute();

        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('GET', '/task/');

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        /* @var $message string */
        $message = "Wow. You have nothing else to do. Enjoy the rest of your day!";

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(View::HTTP_OK, $object->code);
        $this->assertInternalType('string', $object->data);
        $this->assertEquals($object->data, $message);
    }

    /**
     * @test
     * @dataProvider invalidObjects
     * @param array $data
     */
    public function getWithInvalidUuidMustBeResultMethodNotAllowedHttpStatus($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('GET', '/task/' . $data['uuid'], [], [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_METHOD_NOT_ALLOWED, $response->getStatusCode());
        $this->assertEquals(View::HTTP_METHOD_NOT_ALLOWED, $object->code);
        $this->assertInternalType('object', $object->data);
    }

    /**
     * @test
     * @dataProvider validObjects
     */
    public function getWithTaskIdMustBeReturnDetailsOfTask($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('GET', '/task/' . $data['uuid'], [], [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(View::HTTP_OK, $object->code);
        $this->assertInternalType('object', $object->data);
    }

    /**
     * @test
     * @dataProvider notFoundObjects
     */
    public function getWithInexistentThrowsNotFoundHttpStatus($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('GET', '/task/' . $data['uuid'], [], [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        /* @var $message string */
        $message = "Good news! The task you were trying to delete didn't even exist.";

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertEquals(View::HTTP_NOT_FOUND, $object->code);
        $this->assertInternalType('object', $object->data);
        $this->assertEquals($object->data->exception, $message);
    }

    /**
     * @test
     * @dataProvider invalidContents
     */
    public function postWithEmptyTaskMustBeReturnErrorMessageAndBadRequestHttpStatus($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('POST', '/task/', $data, [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        /* @var $message string */
        $message = "Bad move! Try removing the task instead of deleting its content.";

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals(View::HTTP_BAD_REQUEST, $object->code);
        $this->assertInternalType('object', $object->data);
        $this->assertEquals($object->data->exception, $message);
    }

    /**
     * @test
     * @dataProvider duplicatedObjects
     */
    public function postWithDuplicatedUuidTaskMustBeReturnCreatedStatusBecauseUuidIsGeneratedAutomatic($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('POST', '/task/', $data, [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals(View::HTTP_CREATED, $object->code);
        $this->assertInternalType('object', $object->data);
        $this->assertInternalType('string', $object->data->uuid);
        $this->assertNotEquals($data['uuid'], $object->data->uuid);
    }

    /**
     * @test
     * @dataProvider validObjects
     */
    public function postWithValidParametersMustBeReturnCreatedStatus($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('POST', '/task/', $data, [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals(View::HTTP_CREATED, $object->code);
        $this->assertInternalType('object', $object->data);
        $this->assertInternalType('string', $object->data->uuid);
        $this->assertNotEquals($data['uuid'], $object->data->uuid);
    }

    /**
     * @test
     * @dataProvider invalidTypes
     */
    public function postWithInvalidTypeMustBeReturnBadRequestHttpStatusAndErrorMessage($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('POST', '/task/', $data, [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        /* @var $message string */
        $message = "The task type you provided is not supported. You can only use shopping or work.";

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals(View::HTTP_BAD_REQUEST, $object->code);
        $this->assertInternalType('object', $object->data);
        $this->assertEquals($object->data->exception, $message);
    }

    /**
     * @test
     * @dataProvider validObjects
     */
    public function deleteWithExistentTaskMustBeReturnNoContentHttpStatus($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('DELETE', '/task/' . $data['uuid'], [], [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        $this->assertEquals(View::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    /**
     * @test
     * @dataProvider deletedObjects
     */
    public function deleteAlreadyDeletedTaskMustBeReturnBadRequestHttpStatusAndMessage($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('DELETE', '/task/' . $data['uuid'], [], [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        /* @var $message string */
        $message = "Good news! The task you were trying to delete didn't even exist.";

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals(View::HTTP_BAD_REQUEST, $object->code);
        $this->assertInternalType('object', $object->data);
        $this->assertEquals($object->data->exception, $message);
    }

    /**
     * @test
     * @dataProvider nonUpdatableObjects
     */
    public function putWithNonUpdatableObjectsTaskMustBeReturnErrorMessageAndBadRequestHttpStatus($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('PUT', '/task/' . $data['uuid'], $data, [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        /* @var $object \stdClass */
        $object = json_decode($content);

        /* @var $message string */
        $message = "Are you a hacker or something? The task you were trying to edit doesn't exist.";

        $this->assertJson($content);
        $this->assertEquals(View::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals(View::HTTP_BAD_REQUEST, $object->code);
        $this->assertInternalType('object', $object->data);
        $this->assertEquals($object->data->exception, $message);
    }

    /**
     * @test
     * @dataProvider validObjects
     */
    public function putWithValidParametersMustBeReturnCreatedStatus($data)
    {
        /* @var $client \Symfony\Component\HttpKernel\Client */
        $client = $this->createClient();
        $client->request('PUT', '/task/' . $data['uuid'], $data, [], $this->header);

        /* @var $response \Symfony\Component\HttpFoundation\Response */
        $response = $client->getResponse();

        /* @var $content string */
        $content = $response->getContent();

        $this->assertEquals(View::HTTP_NO_CONTENT, $response->getStatusCode());
        $this->assertEmpty($content);
    }
}
