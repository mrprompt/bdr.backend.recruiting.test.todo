<?php
declare(strict_types = 1);

namespace Task\Tests\Controller;

/**
 * Task Controller Provider
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
trait TaskProvider
{
    /**
     * Data Provider
     *
     * @return array
     */
    public function validObjects()
    {
        return [
            [
                [
                    "uuid"      => "d318880a-980f-4d34-98ab-c735bfb22c61",
                    "type"      => "shopping",
                    "order"     => 1,
                    "content"   => uniqid(),
                ]
            ],
            [
                [
                    "uuid"      => "d318880a-980f-4d34-98ab-c735bfb22c62",
                    "type"      => "work",
                    "order"     => 2,
                    "content"   => uniqid(),
                ]
            ],
            [
                [
                    "uuid"      => "d318880a-980f-4d34-98ab-c735bfb22c63",
                    "type"      => "shopping",
                    "order"     => 2,
                    "content"   => uniqid(),
                ]
            ],
            [
                [
                    "uuid"      => "d318880a-980f-4d34-98ab-c735bfb22c64",
                    "type"      => "work",
                    "order"     => 10,
                    "content"   => uniqid(),
                ]
            ],
        ];
    }

    /**
     * Data Provider
     *
     * @return array
     */
    public function notFoundObjects()
    {
        return [
            [
                [
                    "uuid" => "d318880a-980f-4d34-98ab-c735bfb22c81",
                ]
            ],
            [
                [
                    "uuid" => "d318880a-980f-4d34-98ab-c735bfb22c82",
                ]
            ],
            [
                [
                    "uuid" => "d318880a-980f-4d34-98ab-c735bfb22c83",
                ]
            ],
            [
                [
                    "uuid" => "d318880a-980f-4d34-98ab-c735bfb22c85",
                ]
            ],
        ];
    }

    /**
     * Data Provider
     *
     * @return array
     */
    public function invalidObjects()
    {
        return [
            [
                [
                    "uuid"      => "0",
                    "content"   => "",
                    "type"      => "working",
                    "order"     => "A",
                ]
            ],
            [
                [
                    "uuid"      => "0-0-0-0",
                    "content"   => "",
                    "type"      => "foo",
                    "order"     => "-1",
                ]
            ],
            [
                [
                    "uuid"      => "22-22-22-22",
                    "content"   => "",
                    "type"      => 2,
                    "order"     => "",
                ]
            ],
        ];
    }

    /**
     * Data Provider
     *
     * @return array
     */
    public function duplicatedObjects()
    {
        return [
            [
                [
                    "uuid"      => "d318880a-980f-4d34-98ab-c735bfb22c61",
                    "content"   => "Foo",
                    "type"      => "shopping",
                    "order"     => 0,
                ]
            ],
            [
                [
                    "uuid"      => "d318880a-980f-4d34-98ab-c735bfb22c62",
                    "content"   => "Foo",
                    "type"      => "work",
                    "order"     => 1,
                ]
            ],
        ];
    }

    /**
     * Data Provider
     *
     * @return array
     */
    public function invalidTypes()
    {
        return [
            [
                [
                    "uuid"  => "d318880a-980f-4d34-98ab-c735bfb22c61",
                    "type"  => "shoppping",
                    "order" => 1,
                ]
            ],
            [
                [
                    "uuid" => "d318880a-980f-4d34-98ab-c735bfb22c62",
                    "type"  => "working",
                    "order" => 2,
                ]
            ],
            [
                [
                    "uuid" => "d318880a-980f-4d34-98ab-c735bfb22c63",
                    "type"  => "1",
                    "order" => 2,
                ]
            ],
        ];
    }

    /**
     * Data Provider
     *
     * @return array
     */
    public function invalidContents()
    {
        return [
            [
                [
                    "uuid"  => "d318880a-980f-4d34-98ab-c735bfb22c61",
                    "type"  => "work",
                    "content"  => "",
                    "order" => 1,
                ]
            ],
        ];
    }

    /**
     * Data Provider
     *
     * @return array
     */
    public function deletedObjects()
    {
        return [
            [
                [
                    "uuid"  => "d318880a-980f-4d34-98ab-c735bfb22c65",
                ]
            ],
        ];
    }

    /**
     * Data Provider
     *
     * @return array
     */
    public function nonUpdatableObjects()
    {
        return [
            [
                [
                    "uuid"      => "d318880a-980f-4d34-98ab-c735bfb22c65",
                    "type"      => "shopping",
                    "order"     => 1,
                    "content"   => uniqid(),
                ]
            ],
        ];
    }

    /**
     * Data Provider
     * @return array
     */
    public function validOrders()
    {
        return [
            [
                'asc'
            ],
            [
                'desc'
            ],
            [
                'ASC'
            ],
            [
                'DESC'
            ]
        ];
    }
}