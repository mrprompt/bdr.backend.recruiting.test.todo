<?php
declare(strict_types = 1);

namespace Task\Tests\Mock\Service;

use Task\Service\TaskInterface;
use Task\Entity\Task as TaskModel;
use Mockery as m;

/**
 * Task Service Mock
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
abstract class Task
{
    /**
     * @return m\MockInterface
     */
    public static function getMock()
    {
        $model = new TaskModel();

        $task = m::mock(TaskInterface::class);
        $task->shouldReceive('save')->andReturn($model)->byDefault();
        $task->shouldReceive('delete')->andReturn(true)->byDefault();
        $task->shouldReceive('open')->andReturn($model)->byDefault();
        $task->shouldReceive('create')->andReturn($model)->byDefault();
        $task->shouldReceive('update')->andReturn($model)->byDefault();
        $task->shouldReceive('markAsDone')->andReturn($model)->byDefault();
        $task->shouldReceive('findAll')->andReturn([$model])->byDefault();
        $task->shouldReceive('findCompleted')->andReturn([$model])->byDefault();

        return $task;
    }
}
