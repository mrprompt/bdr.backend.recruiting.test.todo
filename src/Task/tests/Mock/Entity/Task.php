<?php
declare(strict_types = 1);

namespace Task\Tests\Mock\Entity;

use Task\Entity\TaskInterface;
use Mockery as m;

/**
 * Task Entity Mock
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
abstract class Task
{
    /**
     * @return m\MockInterface
     */
    public static function getMock()
    {
        $task = m::mock(TaskInterface::class);
        $task->shouldReceive('getId')->andReturn(random_int(0, 100))->byDefault();
        $task->shouldReceive('getUuid')->andReturn(uniqid())->byDefault();
        $task->shouldReceive('setUuid')->andReturnNull()->byDefault();
        $task->shouldReceive('getType')->andReturn(uniqid())->byDefault();
        $task->shouldReceive('setType')->andReturnNull()->byDefault();
        $task->shouldReceive('getContent')->andReturn(uniqid())->byDefault();
        $task->shouldReceive('setContent')->andReturnNull()->byDefault();
        $task->shouldReceive('getOrder')->andReturn(random_int(0, 10))->byDefault();
        $task->shouldReceive('setOrder')->andReturnNull()->byDefault();
        $task->shouldReceive('isDone')->andReturn(rand([true, false]))->byDefault();
        $task->shouldReceive('done')->andReturnNull()->byDefault();
        $task->shouldReceive('getCreated')->andReturn(new \DateTime())->byDefault();
        $task->shouldReceive('getUpdated')->andReturn(new \DateTime())->byDefault();

        return $task;
    }
}
