<?php
declare(strict_types = 1);

namespace Task\Tests\Mock\Repository;

use Common\ChangeProtectedAttribute;
use Task\Repository\TaskInterface;
use Task\Entity\Task as TaskModel;
use Mockery as m;

/**
 * Task Repository Mock
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
abstract class Task
{
    use ChangeProtectedAttribute;

    /**
     * @return m\MockInterface
     */
    public static function getMock()
    {
        $taskModel = new TaskModel();
        $taskModel->delete(false);

        $taskDeleted = clone $taskModel;
        $taskDeleted->delete(true);

        $task = m::mock(TaskInterface::class);

        $task
            ->shouldReceive('findById')
            ->andReturn($taskModel)
            ->byDefault();

        $task
            ->shouldReceive('findById')
            ->with('1000-0000-0000-000')
            ->andReturn($taskDeleted)
            ->byDefault();

        $task->shouldReceive('findAll')->andReturn([$taskModel])->byDefault();

        $task->shouldReceive('findCompleted')->andReturn([$taskModel])->byDefault();

        return $task;
    }
}
