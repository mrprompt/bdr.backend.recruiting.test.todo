<?php
declare(strict_types = 1);

namespace Task\Tests\Value;

use Common\ChangeProtectedAttribute;
use PHPUnit_Framework_TestCase;
use Task\Value\Type;

/**
 * Task Type test case.
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
class TypeTest extends PHPUnit_Framework_TestCase
{
    use ChangeProtectedAttribute;

    /**
     * @test
     * @covers \Task\Value\Type::__construct()
     */
    public function createTypeWithoutParamsMustInstanceObject()
    {
        $type   = new Type();
        
        $this->assertInstanceOf(Type::class, $type);
    }

    /**
     * @test
     * @param string $types
     * @covers \Task\Value\Type::__construct()
     * @dataProvider validTypes
     */
    public function createTypeWithValidTypeMustBeInstanceObject(string $types)
    {
        $type   = new Type($types);

        $this->assertInstanceOf(Type::class, $type);
    }

    /**
     * @test
     * @param string $types
     * @covers \Task\Value\Type::__construct()
     * @dataProvider invalidTypes
     * @expectedException \OutOfBoundsException
     */
    public function createTypeWithInvalidTypeThrowsException(string $types)
    {
        $type = new Type($types);
    }

    /**
     * @test
     * @covers \Task\Value\Type::__construct()
     * @covers \Task\Value\Type::getType()
     */
    public function getTypeMustBeReturnTypeAttribute()
    {
        $type = new Type();
        
        $this->modifyAttribute($type, 'type', 'work');

        $result = $type->getType();
        
        $this->assertTrue(is_string($result));
        $this->assertEquals('work', $result);
    }

    /**
     * @test
     * @covers \Task\Value\Type::__construct()
     * @covers \Task\Value\Type::__toString()
     * @dataProvider validTypes
     */
    public function toStringMustBeReturnTypeAttribute($types)
    {
        $type = new Type($types);

        $result = $type->__toString();

        $this->assertEquals($types, $result);
    }

    /**
     * Data Provider
     * @return array
     */
    public function validTypes()
    {
        return [
            [
                'work'
            ],
            [
                'shopping'
            ],
        ];
    }

    /**
     * Data Provider
     * @return array
     */
    public function invalidTypes()
    {
        return [
            [
                'test'
            ],
            [
                ''
            ],
        ];
    }
}
