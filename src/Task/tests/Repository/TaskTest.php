<?php
declare(strict_types = 1);

namespace Task\Tests\Repository;

use Common\ChangeProtectedAttribute;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Tests\OrmTestCase;
use Task\Entity\Task;

/**
 * @author Thiago Paes <mrprompt@gmail.com>
 */
class TaskTest extends OrmTestCase
{
    use ChangeProtectedAttribute;

    /**
     * @var Task
     */
    protected $obj;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * Bootstrap
     */
    public function setUp()
    {
        parent::setUp();

        $reader = new AnnotationReader();
        $metadataDriver = new AnnotationDriver($reader, Task::class);

        $this->em = $this->_getTestEntityManager();
        $this->em->getConfiguration()->setMetadataDriverImpl($metadataDriver);

        $this->obj = $this->em->getRepository(Task::class);
    }

    /**
     * Shutdown
     */
    public function tearDown()
    {
        $this->obj = null;

        parent::tearDown();
    }

    /**
     * @test
     * @covers \Task\Repository\Task::findById
     */
    public function findById()
    {
        $result = $this->obj->findById('0-0-0-0');

        $this->assertTrue(is_object($result));
    }

    /**
     * @test
     * @covers \Task\Repository\Task::findAll
     */
    public function findAll()
    {
        $result = $this->obj->findAll();

        $this->assertTrue(is_array($result));
    }

    /**
     * @test
     * @covers \Task\Repository\Task::findCompleted
     */
    public function findCompleted()
    {
        $result = $this->obj->findCompleted();

        $this->assertTrue(is_array($result));
    }
}
