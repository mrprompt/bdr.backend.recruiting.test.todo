<?php
declare(strict_types = 1);

namespace Task\Tests\Service;

use Common\ChangeProtectedAttribute;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Tests\OrmTestCase;
use Task\Entity\Task;
use Task\Entity\TaskInterface;
use Task\Service\Task as TaskService;
use Task\Tests\Mock\Repository\Task as CreateTaskRepositoryMock;

/**
 * Task service test case.
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
class TaskTest extends OrmTestCase
{
    use ChangeProtectedAttribute;

    /**
     * @var TaskService
     */
    private $obj;

    /**
     * Boot
     */
    public function setUp()
    {
        parent::setUp();

        $reader = new AnnotationReader();
        $metadataDriver = new AnnotationDriver($reader, Task::class);

        $em = $this->_getTestEntityManager();
        $em->getConfiguration()->setMetadataDriverImpl($metadataDriver);

        $this->obj = new TaskService($em);

        $this->modifyAttribute($this->obj, 'tasks', CreateTaskRepositoryMock::getMock());
    }

    /**
     * Shutdown
     */
    public function tearDown()
    {
        $this->obj = null;

        parent::tearDown();
    }

    /**
     * @test
     * @covers \Task\Service\Task::save()
     */
    public function saveMustBeReturnSameObject()
    {
        $taskModel = new Task();

        $result = $this->obj->save($taskModel);

        $this->assertSame($taskModel, $result);
    }

    /**
     * @test
     * @covers \Task\Service\Task::delete()
     */
    public function deleteMustBeReturnTrueWhenSuccess()
    {
        $result = $this->obj->delete('0000-0000-0000-000');

        $this->assertTrue($result);
    }

    /**
     * @test
     * @covers \Task\Service\Task::delete()
     * @expectedException \InvalidArgumentException
     */
    public function deleteInexistentMustBeThrowsException()
    {
        $result = $this->obj->delete('1000-0000-0000-000');

        $this->assertTrue($result);
    }

    /**
     * @test
     * @covers \Task\Service\Task::open()
     */
    public function openWithValidUuidAndUserMustBeReturnTaskObject()
    {
        $result = $this->obj->open('0-0-0-0');

        $this->assertInstanceOf(TaskInterface::class, $result);
    }

    /**
     * @test
     * @covers \Task\Service\Task::findAll()
     */
    public function findAllMustBeReturnArray()
    {
        $result = $this->obj->findAll();

        $this->assertTrue(is_array($result));
    }

    /**
     * @test
     * @covers \Task\Service\Task::findCompleted()
     */
    public function findCompletedMustBeReturnArray()
    {
        $result = $this->obj->findCompleted();

        $this->assertTrue(is_array($result));
    }

    /**
     * @test
     * @covers \Task\Service\Task::create()
     */
    public function createMustBeReturnTaskObjectWhenReceiveAllCorrectParams()
    {
        $result = $this->obj->create('0-0-0-0', 'shopping', uniqid(), 0);

        $this->assertInstanceOf(TaskInterface::class, $result);
    }

    /**
     * @test
     * @covers \Task\Service\Task::update()
     */
    public function updateMustBeReturnSameObject()
    {
        $result = $this->obj->update('0-0-0-0', 'work', uniqid(), 0);

        $this->assertInstanceOf(TaskInterface::class, $result);
    }
}
