<?php
declare(strict_types = 1);

namespace Task\Tests\Entity;

use Common\ChangeProtectedAttribute;
use Task\Entity\Task;
use PHPUnit_Framework_TestCase;
use Mockery;
use Task\Value\Type;

/**
 * Task test case.
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
class TaskTest extends PHPUnit_Framework_TestCase
{
    use ChangeProtectedAttribute;

    /**
     * the Task object
     * @var Task
     */
    private $object;

    /**
     * Set Up the task
     */
    public function setUp()
    {
        parent::setUp();

        $this->object = new Task();
    }

    /**
     * Shutdown the test
     */
    public function tearDown()
    {
        $this->object = null;
        
        parent::tearDown();
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::getId()
     */
    public function getIdMustBeReturnIdProperty()
    {
        $this->modifyAttribute($this->object, 'id', 1);
        
        $result = $this->object->getId();
        
        $this->assertEquals(1, $result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::getUuid()
     * @dataProvider validObjects
     */
    public function getUuidMustBeReturnUuidProperty($data)
    {
        $this->modifyAttribute($this->object, 'uuid', $data['uuid']);

        $result = $this->object->getUuid();

        $this->assertEquals($data['uuid'], $result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::setUuid()
     * @dataProvider validObjects
     */
    public function setUuidMustBeReturnNullWhenReceiveUuidValue($data)
    {
        $result = $this->object->setUuid($data['uuid']);

        $this->assertEmpty($result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::setUuid()
     * @dataProvider invalidObjects
     * @expectedException \InvalidArgumentException
     */
    public function setUuidThrowsExceptionWhenReceiveInvalidValue($data)
    {
        $this->object->setUuid($data['uuid']);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::getType()
     * @dataProvider validObjects
     */
    public function getTypeMustBeReturnTypeProperty($data)
    {
        $this->modifyAttribute($this->object, 'type', $data['type']);

        $result = $this->object->getType();

        $this->assertEquals($data['type'], $result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::setType()
     * @dataProvider validObjects
     */
    public function setTypeMustBeReturnNullWhenReceiveTypeValue($data)
    {
        $result = $this->object->setType($data['type']);

        $this->assertEmpty($result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::setType()
     * @dataProvider invalidObjects
     * @expectedException \TypeError
     */
    public function setTypeThrowsExceptionWhenReceiveInvalidValue($data)
    {
        $this->object->setType($data['type']);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::getContent()
     * @dataProvider validObjects
     */
    public function getContentMustBeReturnContentProperty($data)
    {
        $this->modifyAttribute($this->object, 'content', $data['content']);

        $result = $this->object->getContent();

        $this->assertEquals($data['content'], $result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::setContent()
     * @dataProvider validObjects
     */
    public function setContentMustBeReturnNullWhenReceiveContentValue($data)
    {
        $result = $this->object->setContent($data['content']);

        $this->assertEmpty($result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::setContent()
     * @dataProvider invalidObjects
     * @expectedException \InvalidArgumentException
     */
    public function setContentThrowsExceptionWhenReceiveInvalidValue($data)
    {
        $this->object->setContent($data['content']);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::getOrder()
     * @dataProvider validObjects
     */
    public function getOrderMustBeReturnOrderProperty($data)
    {
        $this->modifyAttribute($this->object, 'order', $data['order']);

        $result = $this->object->getOrder();

        $this->assertEquals($data['order'], $result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::setOrder()
     * @dataProvider validObjects
     */
    public function setOrderMustBeReturnNullWhenReceiveOrderValue($data)
    {
        $result = $this->object->setOrder($data['order']);

        $this->assertEmpty($result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::setOrder()
     * @dataProvider invalidObjects
     * @expectedException \InvalidArgumentException
     */
    public function setOrderThrowsExceptionWhenReceiveInvalidValue($data)
    {
        $this->object->setOrder($data['order']);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::isDone()
     * @dataProvider validObjects
     */
    public function isDoneMustBeReturnDoneProperty($data)
    {
        $this->modifyAttribute($this->object, 'done', $data['done']);

        $result = $this->object->isDone();

        $this->assertTrue(is_bool($result));
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::done()
     * @dataProvider validObjects
     */
    public function doneMustBeReturnNullWhenReceiveDoneValue($data)
    {
        $result = $this->object->done($data['done']);

        $this->assertEmpty($result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::done()
     * @dataProvider invalidObjects
     * @expectedException \TypeError
     */
    public function doneThrowsExceptionWhenReceiveInvalidValue($data)
    {
        $this->object->done($data['done']);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::getCreated()
     * @dataProvider validObjects
     */
    public function getCreatedMustBeReturnCreatedProperty($data)
    {
        $this->modifyAttribute($this->object, 'created', $data['created']);

        $result = $this->object->getCreated();

        $this->assertSame($data['created'], $result);
    }

    /**
     * @test
     * @covers \Task\Entity\Task::__construct()
     * @covers \Task\Entity\Task::getUpdated()
     * @dataProvider validObjects
     */
    public function getUpdatedMustBeReturnUpdatedProperty($data)
    {
        $this->modifyAttribute($this->object, 'updated', $data['updated']);

        $result = $this->object->getUpdated();

        $this->assertSame($data['updated'], $result);
    }

    /**
     * Valid data
     * @return array
     */
    public function validObjects(): array
    {
        return [
            [
                [
                    'uuid' => uniqid(),
                    'type' => new Type('work'),
                    'content' => uniqid(),
                    'order' => 0,
                    'done' => true,
                    'created' => new \DateTime(),
                    'updated' => new \DateTime(),
                ]
            ],
            [
                [
                    'uuid' => uniqid(),
                    'type' => new Type('shopping'),
                    'content' => uniqid(),
                    'order' => 1,
                    'done' => false,
                    'created' => new \DateTime(),
                    'updated' => new \DateTime(),
                ]
            ],
        ];
    }

    /**
     * Invalid data
     * @return array
     */
    public function invalidObjects(): array
    {
        return [
            [
                [
                    'uuid' => '',
                    'type' => '',
                    'content' => '',
                    'order' => -1,
                    'done' => '',
                    'created' => date('Y-m-d'),
                    'updated' => date('Y-m-d'),
                ]
            ],
            [
                [
                    'uuid' => '',
                    'type' => '',
                    'content' => '',
                    'order' => -2,
                    'done' => 'true',
                    'created' => time(),
                    'updated' => time(),
                ]
            ],
            [
                [
                    'uuid' => '',
                    'type' => '',
                    'content' => '',
                    'order' => -1,
                    'done' => 0,
                    'created' => '1982-06-03',
                    'updated' => '1982-06-03',
                ]
            ],
            [
                [
                    'uuid' => '',
                    'type' => '',
                    'content' => '',
                    'order' => -2,
                    'done' => 1,
                    'created' => '',
                    'updated' => '',
                ]
            ],
        ];
    }
}
