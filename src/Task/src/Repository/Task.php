<?php
declare(strict_types = 1);

namespace Task\Repository;

use Doctrine\ORM\EntityRepository;
use Task\Entity\Task as TaskModel;

/**
 * Task Repository
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
final class Task extends EntityRepository implements TaskInterface
{
    /**
     * @inheritdoc
     */
    public function findById(string $uuid): TaskModel
    {
        $task = $this->findOneBy(
            [
                'uuid'      => $uuid,
                'deleted'   => false
            ]
        );

        if (null === $task) {
            $task = new TaskModel();
            $task->delete(true);
            $task->done(true);
        }

        return $task;
    }

    /**
     * @inheritdoc
     */
    public function findAll(string $order = 'ASC'): array
    {
        $tasks = $this->findBy(
            [
                'deleted'   => false
            ],
            [
                'order'     => $order,
                'created'   => 'DESC',
                'updated'   => 'DESC'
            ]
        );

        return $tasks;
    }

    /**
     * @inheritdoc
     */
    public function findCompleted(): array
    {
        $tasks = $this->findBy(
            [
                'done'      => true,
                'deleted'   => false
            ],
            [
                'order'     => 'ASC',
                'created'   => 'DESC',
                'updated'   => 'DESC'
            ]
        );

        return $tasks;
    }

    /**
     * @inheritdoc
     */
    public function findPending(): array
    {
        $tasks = $this->findBy(
            [
                'done'      => false,
                'deleted'   => false
            ],
            [
                'order'     => 'ASC',
                'created'   => 'DESC',
                'updated'   => 'DESC'
            ]
        );

        return $tasks;
    }
}
