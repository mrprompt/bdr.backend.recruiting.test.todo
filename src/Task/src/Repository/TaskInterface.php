<?php
declare(strict_types = 1);

namespace Task\Repository;

use Task\Entity\Task as TaskModel;

/**
 * Task Repository interface
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
interface TaskInterface
{
    /**
     * Get task
     *
     * @param string $uuid
     * @return TaskModel
     */
    public function findById(string $uuid): TaskModel;

    /**
     * List all tasks
     *
     * @return array
     */
    public function findAll(): array;

    /**
     * List all completed tasks
     *
     * @return array
     */
    public function findCompleted(): array;

    /**
     * List all pending tasks
     *
     * @return array
     */
    public function findPending(): array;
}
