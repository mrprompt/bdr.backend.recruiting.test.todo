<?php
declare(strict_types = 1);

namespace Task\Value;

use Respect\Validation\Exceptions\AllOfException;
use Respect\Validation\Validator;

/**
 * Type object value
 * @package Task\Tests\Controller
 * @author Thiago Paes <mrprompt@gmail.com>
 */
final class Type
{
    /**
     * Valid types
     * @var array
     */
    private $types = ['shopping', 'work'];

    /**
     * Defined type
     * @var string
     */
    private $type;

    /**
     * Type constructor.
     * @param string $type
     */
    public function __construct(string $type = 'work')
    {
        try {
            Validator::in($this->types)->assert($type);

            $this->type = $type;
        } catch (AllOfException $ex) {
            $message = "The task type you provided is not supported. You can only use shopping or work.";

            throw new \OutOfBoundsException($message);
        }
    }

    /**
     * Get type type
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Object to string, return type
     * @return string
     */
    public function __toString()
    {
        return $this->type;
    }
}