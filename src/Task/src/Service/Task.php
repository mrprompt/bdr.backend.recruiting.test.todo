<?php
declare(strict_types = 1);

namespace Task\Service;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Task\Entity\Task as TaskModel;
use Exception;
use InvalidArgumentException;
use Task\Value\Type;

/**
 * Task Service
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
final class Task implements TaskInterface
{
    /**
     * @var \Task\Repository\TaskInterface
     */
    private $tasks;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Task Service Constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em       = $em;
        $this->tasks    = $em->getRepository(TaskModel::class);
    }

    /**
     * @inheritdoc
     */
    public function save(TaskModel $task): TaskModel
    {
        $this->em->beginTransaction();

        try {
            $this->em->persist($task);
            $this->em->flush();
            $this->em->commit();

            return $task;
        } catch (UniqueConstraintViolationException $ex) {
            $this->em->rollBack();

            throw new InvalidArgumentException('A Task with this uuid is already registered', 409, $ex);
        } catch (Exception $ex) {
            $this->em->rollBack();

            throw new InvalidArgumentException('An error occurred trying save task', 500, $ex);
        }
    }

    /**
     * @inheritdoc
     */
    public function delete(string $uuid): bool
    {
        $task = $this->open($uuid);

        if ($task->isDeleted()) {
            throw new InvalidArgumentException('Good news! The task you were trying to delete didn\'t even exist.');
        }

        $task->delete(true);

        $this->save($task);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function open(string $uuid): TaskModel
    {
        return $this->tasks->findById($uuid);
    }

    /**
     * @inheritdoc
     */
    public function findAll(string $order = 'ASC'): array
    {
        $tasks = $this->tasks->findAll($order);

        return $tasks;
    }

    /**
     * @inheritdoc
     */
    public function findPending(): array
    {
        $tasks = $this->tasks->findPending();

        return $tasks;
    }

    /**
     * @inheritdoc
     */
    public function findCompleted(): array
    {
        $tasks = $this->tasks->findCompleted();

        return $tasks;
    }

    /**
     * @inheritdoc
     */
    public function create(string $uuid, string $type, string $content, int $order): TaskModel
    {
        $task = new TaskModel();
        $task->setUuid($uuid);
        $task->setType(new Type($type));
        $task->setContent($content);
        $task->setOrder($order);

        return $this->save($task);
    }

    /**
     * @inheritdoc
     */
    public function update(string $uuid, string $type, string $content, int $order): TaskModel
    {
        $task = $this->open($uuid);

        if ($task->isDeleted()) {
            $error = 'Are you a hacker or something? The task you were trying to edit doesn\'t exist.';

            throw new InvalidArgumentException($error);
        }

        $task->setType(new Type($type));
        $task->setContent($content);
        $task->setOrder($order);

        return $this->save($task);
    }

    /**
     * @inheritdoc
     */
    public function markAsDone(string $uuid): TaskModel
    {
        $task = $this->open($uuid);
        $task->done(true);

        return $this->save($task);
    }
}
