<?php
declare(strict_types = 1);

namespace Task\Service;

use Task\Entity\Task as TaskModel;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Task Service
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
interface TaskInterface
{
    /**
     * Task Service Constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em);

    /**
     * Save task
     * @param  TaskModel $task
     * @return TaskModel
     */
    public function save(TaskModel $task): TaskModel;

    /**
     * Remove Task
     * @param string $uuid
     * @return bool
     */
    public function delete(string $uuid): bool;

    /**
     * Find one by task id
     *
     * @param string $uuid
     * @return TaskModel
     */
    public function open(string $uuid): TaskModel;

    /**
     * List all tasks
     *
     * @return array
     */
    public function findAll(): array;

    /**
     * List all pending tasks
     *
     * @return array
     */
    public function findPending(): array;

    /**
     * List all completed tasks
     *
     * @return array
     */
    public function findCompleted(): array;

    /**
     * Create a task
     *
     * @param string $uuid
     * @param string $type
     * @param string $content
     * @param int $order
     * @return TaskModel
     */
    public function create(string $uuid, string $type, string $content, int $order): TaskModel;

    /**
     * Update a task
     *
     * @param string $uuid
     * @param string $type
     * @param string $content
     * @param int $order
     * @return TaskModel
     */
    public function update(string $uuid, string $type, string $content, int $order): TaskModel;

    /**
     * Update a task
     *
     * @param string $uuid
     * @return TaskModel
     */
    public function markAsDone(string $uuid): TaskModel;
}
