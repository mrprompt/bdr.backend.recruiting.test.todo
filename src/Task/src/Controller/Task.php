<?php
declare(strict_types = 1);

namespace Task\Controller;

use Silex\Application;
use Common\Response as View;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Task Controller
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
final class Task
{
    /**
     * Route to /task/
     *
     * @param  Application $app
     * @return View
     */
    public function get(Application $app): View
    {
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $request = $app['request'];
        $order   = (string) strtoupper($request->get('ord', 'ASC'));

        $tasks = $app['task.service']->findAll($order);

        // because the service always return array, needs treat counter
        if (count($tasks) === 0) {
            $tasks = "Wow. You have nothing else to do. Enjoy the rest of your day!";
        }

        return new View($tasks, View::HTTP_OK);
    }

    /**
     * Retrieves information from task
     *
     * @param  Application $app
     * @return View
     */
    public function view(Application $app): View
    {
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $request = $app['request'];
        $id      = (string) $request->get('id');

        $task    = $app['task.service']->open($id);

        // because service always return an object, is necessary treat not found manually
        if ($task->getId() === 0) {
            throw new NotFoundHttpException("Good news! The task you were trying to delete didn't even exist.");
        }

        return new View($task, View::HTTP_OK);
    }

    /**
     * Create task
     *
     * @param  Application $app
     * @return View
     */
    public function create(Application $app): View
    {
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $request = $app['request'];
        $content = (string) $request->get('content');
        $order   = (int) $request->get('order');
        $type    = (string) $request->get('type');
        $uuid    = (string) $app['uuid.generate']();

        try {
            $task = $app['task.service']->create($uuid, $type, $content, $order);

            return new View($task, View::HTTP_CREATED);
        } catch (\OutOfBoundsException $exception) {
            $error = "The task type you provided is not supported. You can only use shopping or work.";

            throw new BadRequestHttpException($error);
        } catch (\InvalidArgumentException $exception) {
            $error = "Bad move! Try removing the task instead of deleting its content.";

            throw new BadRequestHttpException($error);
        }
    }

    /**
     * Delete task
     *
     * @param  Application $app
     * @return View
     */
    public function delete(Application $app): View
    {
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $request = $app['request'];
        $id      = (string) $request->get('id');

        try {
            $task = $app['task.service']->delete($id);

            return new View($task, View::HTTP_NO_CONTENT);
        } catch (\InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }
    }

    /**
     * Create task
     *
     * @param  Application $app
     * @return View
     */
    public function update(Application $app): View
    {
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $request = $app['request'];
        $uuid    = (string) $request->get('id');
        $content = (string) $request->get('content');
        $order   = (int) $request->get('order');
        $type    = (string) $request->get('type');

        try {
            $task = $app['task.service']->update($uuid, $type, $content, $order);

            return new View($task, View::HTTP_NO_CONTENT);
        } catch (\InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }
    }
}
