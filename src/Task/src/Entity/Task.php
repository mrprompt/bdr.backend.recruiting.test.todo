<?php
declare(strict_types = 1);

namespace Task\Entity;

use Common\Entity;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use JMS\Serializer\Annotation as Serializer;
use Respect\Validation\Exceptions\AllOfException;
use Respect\Validation\Validator as v;
use Task\Value\Type;

/**
 * Task Entity
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 *
 * @ORM\Table(name="task", uniqueConstraints={@ORM\UniqueConstraint(name="uuid_idx", columns={"uuid"})})
 * @ORM\Entity(repositoryClass="\Task\Repository\Task")
 * @ORM\HasLifecycleCallbacks
 */
class Task implements TaskInterface
{
    use Entity;

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Type("integer")
     * @Serializer\Exclude
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="uuid", type="string", length=140, nullable=false, unique=true)
     * @Serializer\Type("string")
     * @var string
     */
    protected $uuid;

    /**
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     * @Serializer\Type("string")
     * @var string
     */
    protected $type;

    /**
     * @ORM\Column(name="content", type="text", nullable=true)
     * @Serializer\Type("string")
     * @var string
     */
    protected $content;

    /**
     * @ORM\Column(name="sort_order", type="integer", nullable=true, options={"default":0})
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("sort_order")
     * @var integer
     */
    protected $order;

    /**
     * @ORM\Column(name="done", type="boolean", nullable=true, options={"default":false})
     * @Serializer\Type("boolean")
     * @var bool
     */
    protected $done;

    /**
     * @ORM\Column(name="deleted", type="boolean", nullable=true, options={"default":false})
     * @Serializer\Type("boolean")
     * @Serializer\Exclude
     * @var bool
     */
    protected $deleted;

    /**
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Serializer\Type("DateTime")
     * @var DateTime
     */
    protected $created;

    /**
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Serializer\Type("DateTime")
     * @var DateTime
     */
    protected $updated;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id       = 0;
        $this->uuid     = '';
        $this->type     = new Type();
        $this->content  = '';
        $this->order    = 0;
        $this->done     = false;
        $this->deleted  = false;
        $this->created  = new DateTime();
        $this->updated  = new DateTime();
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @inheritDoc
     */
    public function setUuid(string $uuid)
    {
        try {
            v::notEmpty()->assert($uuid);

            $this->uuid = $uuid;
        } catch (AllOfException $exception) {
            throw new InvalidArgumentException('Incorrect UUID value');
        }
    }

    /**
     * @inheritDoc
     */
    public function getType(): Type
    {
        return $this->type ?: new Type();
    }

    /**
     * @inheritDoc
     */
    public function setType(Type $type)
    {
        $this->type = $type;
    }

    /**
     * @inheritDoc
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @inheritDoc
     */
    public function setContent(string $content)
    {
        try {
            v::notEmpty()->assert($content);

            $this->content = $content;
        } catch (AllOfException $exception) {
            throw new InvalidArgumentException('Invalid Content value');
        }
    }

    /**
     * @inheritDoc
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @inheritDoc
     */
    public function setOrder(int $order)
    {
        try {
            v::min(0)->assert($order);

            $this->order = $order;
        } catch (AllOfException $exception) {
            throw new InvalidArgumentException('Invalid Order number');
        }
    }

    /**
     * @inheritDoc
     */
    public function isDone(): bool
    {
        return $this->done === true;
    }

    /**
     * @inheritDoc
     */
    public function done(bool $done)
    {
        $this->done = $done;
    }

    /**
     * @inheritDoc
     */
    public function isDeleted(): bool
    {
        return $this->deleted === true;
    }

    /**
     * @inheritDoc
     */
    public function delete(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @inheritDoc
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @inheritDoc
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }
}
