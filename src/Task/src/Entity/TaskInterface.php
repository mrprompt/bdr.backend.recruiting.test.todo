<?php
declare(strict_types = 1);

namespace Task\Entity;

use Task\Value\Type;

/**
 * Task Entity Interface
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 */
interface TaskInterface
{
    /**
     * Get the Id
     * @return int
     */
    public function getId(): int;

    /**
     * Get the UUID
     * @return string
     */
    public function getUuid(): string;

    /**
     * Set the UUID
     * @param string $uuid
     * @return null
     */
    public function setUuid(string $uuid);

    /**
     * Get the type
     * @return Type
     */
    public function getType(): Type;

    /**
     * Set the type
     * @param Type $type
     * @return null
     */
    public function setType(Type $type);

    /**
     * Get the content
     * @return string
     */
    public function getContent(): string;

    /**
     * Set the content
     * @param string $content
     * @return null
     */
    public function setContent(string $content);

    /**
     * Get the order
     * @return int
     */
    public function getOrder(): int;

    /**
     * Set the order
     * @param int $order
     * @return mixed
     */
    public function setOrder(int $order);

    /**
     * Check if task is done
     * @return bool
     */
    public function isDone(): bool;

    /**
     * Set task as done
     * @param bool $done
     * @return mixed
     */
    public function done(bool $done);

    /**
     * Check if task is deleted
     * @return bool
     */
    public function isDeleted(): bool;

    /**
     * Set task as deleted
     * @param bool $done
     * @return mixed
     */
    public function delete(bool $done);

    /**
     * Get the created time
     * @return \DateTime
     */
    public function getCreated(): \DateTime;

    /**
     * Get the updated time
     * @return \DateTime
     */
    public function getUpdated(): \DateTime;
}
